#include "knnmainwindow.h"
#include "ui_knnmainwindow.h"

KNNMainWindow::KNNMainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::KNNMainWindow)
{
    ui->setupUi(this);
}

KNNMainWindow::~KNNMainWindow()
{
    delete ui;
}


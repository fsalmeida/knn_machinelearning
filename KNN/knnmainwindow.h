#ifndef KNNMAINWINDOW_H
#define KNNMAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class KNNMainWindow; }
QT_END_NAMESPACE

class KNNMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    KNNMainWindow(QWidget *parent = nullptr);
    ~KNNMainWindow();

private:
    Ui::KNNMainWindow *ui;
};
#endif // KNNMAINWINDOW_H
